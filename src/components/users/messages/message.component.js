import React, { Component } from 'react';
import * as io from 'socket.io-client';
import './message.component.css';
import util from '../../../utils/util';
import notify from '../../../utils/notify';
export default class MessageComponent extends Component {
    currentUser = {}
    constructor() {
        super();
        this.state = {
            data: {
                msg: '',
                senderId: '',
                receiverId: '',
                senderName: '',
                receiverName: '',
                time: '',
            },
            messages: [],
            users: []
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }));
    }

    handleSubmit = e => {
        e.preventDefault();
        if (!this.state.data.receiverId) {
            return notify.showInfo('Please Select User to continue');
        }
        const { data } = this.state
        data.time = new Date();
        this.socket.emit('new-msg', data);
    }

    selectUser(user) {
        console.log('user is >>', user);
        this.setState((pre) => ({
            data: {
                ...pre.data,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    componentDidMount() {
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.setState((pre) => ({
            data: {
                ...pre.data,
                senderName: this.currentUser.username,
            }
        }))
        this.socket = io(process.env.REACT_APP_SOCKET_URL);
        this.socket.emit('new-user', this.currentUser.username);

        this.runSocket();
    }

    runSocket() {
        this.socket.on('reply-msg', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages,
            })
        });
        this.socket.on('reply-msg-to', (data) => {

            const { messages } = this.state;
            messages.push(data);
            this.setState((pre) => ({
                data: {
                    ...pre.data,
                    receiverId: data.senderId
                },
                messages,
            }))
        });
        this.socket.on('users', (data) => {
            console.log(data);
            let senderId;
            data.forEach((user, i) => {
                console.log(this.currentUser.username)
                if (user.name === this.currentUser.username) {
                    senderId = user.id;
                }
            });
            this.setState((pre) => ({
                data: {
                    ...pre.data,
                    senderId
                },
                users: data
            }));
        })

    }
    render() {
        let msgContent = this.state.messages.map((msg, i) => (
            <li key={i}>
                <p className="sender">{msg.senderName}</p>
                <h3>{msg.msg}</h3>
                <small>{util.formatTime(msg.time)}</small>
            </li>
        ))
        let usersContent = this.state.users.map((user, i) => (
            <li key={i}>
                <button className="btn btn-default" onClick={() => { this.selectUser(user) }}>{user.name}</button>
            </li>
        ))
        return (
            <div>
                <h2>Let's chat <strong>{this.state.data.senderName}</strong></h2>
                <div className="row">
                    <div className="col-md-5">
                        <ins>Messages</ins>
                        <div className="message-box">
                            <ul>
                                {msgContent}
                            </ul>
                        </div>
                        <form className="form-group" onSubmit={this.handleSubmit}>
                            <input placeholder="Enter your message" name="msg" className="form-control" onChange={this.handleChange}></input>
                            <button className="btn btn-success" type="submit">send</button>
                        </form>
                    </div>
                    <div className="col-md-2"></div>
                    <div className="col-md-5">
                        <ins>Users</ins>
                        <div className="users-list">
                            <ul>
                                {usersContent}
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
