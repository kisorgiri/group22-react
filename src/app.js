import React from 'react';
import AppRoutes from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from './store';
// functional component
function Welcome(props) {
    // args or arguments of functions are props to welcome component

    // return block must be there inside function
    // html node should be returned from component
    return (
        <div>
            <Provider store={store}>
                <AppRoutes></AppRoutes>
                <ToastContainer></ToastContainer>
            </Provider>
        </div >
    )
}

export const App = Welcome;

// component
// component basic building block of react
// component can be functional as well as class based component
// component can be stateless and statefull
// state is scope(data ) within a component
// props incoming data to components

// react hooks
// functional componente can maintain state

// in general for our course
// class based componente will hold state (stateful component)
// functional component will be 