import React, { Component } from 'react'
import { httpClient } from '../../../utils/httpClient'
import notify from '../../../utils/notify'
import { ViewProductComponent } from '../view-products/view-prouduct.component';

export class SearchProduct extends Component {

    constructor() {
        super();
        this.state = {
            data: {},
            error: {},
            categories: [],
            names: [],
            allData: [],
            isLoading: false,
            isSubmitting: false,
            searchResults: [],
            validForm: true
        };
    }

    handleChange = e => {
        let { name, value, type, checked } = e.target;
        if (name === 'category') {
            this.filterNames(value);
        }
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const data = this.state.data;
        if (!data.multipleDateRange) {
            data.toDate = null;
        }
        if (!data.toDate) {
            data.toDate = data.fromDate
        }
        httpClient.post('/product/search', data)
            .then(data => {
                if (!data.data.length) {
                    return notify.showInfo('No any products matched your search query');
                }
                console.log('data >>', data);
                this.setState({
                    searchResults: data.data
                });
            })
            .catch(err => {
                notify.handleError(err);
            })
    }
    searchAgain = () => {
        this.setState({
            searchResults: [],
            data: {}
        })
    }

    filterNames(categoryName) {
        const { allData } = this.state;
        let names = allData.filter(item => item.category === categoryName);
        this.setState({
            names
        });
    }

    componentDidMount() {
        httpClient.post('/product/search', {})
            .then(data => {
                // console.log('data is >>', data);
                let categories = [];
                (data.data || []).forEach((item, i) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category);
                    }
                })
                this.setState({
                    categories,
                    allData: data.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            });
    }
    render() {
        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">submit</button>

        let categoryOptions = this.state.categories.map((item, i) => (
            <option key={i} value={item}>{item}</option>
        ));
        let nameOptions = this.state.names.map((item, i) => (
            <option key={i} value={item.name}>{item.name}</option>
        ));

        let nameContent = this.state.data.category
            ? <>
                <label>Name</label>
                <select className="form-control" name="name" onChange={this.handleChange}>
                    <option value="">(Select Name)</option>
                    {nameOptions}
                </select>
            </>
            : '';

        let toDateContent = this.state.data.multipleDateRange
            ? <>
                <label>To Date</label>
                <input className="form-control" type="date" name="toDate" onChange={this.handleChange}></input>
            </>
            : '';

        let mainContent = this.state.searchResults.length > 0
            ? <>
                <button className="btn btn-success" onClick={this.searchAgain}>search again</button>
                <ViewProductComponent productData={this.state.searchResults}></ViewProductComponent>
            </>
            : <>
                <h2>Search</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Category</label>
                    <select className="form-control" name="category" onChange={this.handleChange}>
                        <option value="">(Select Category)</option>
                        {categoryOptions}
                    </select>
                    {nameContent}
                    <label>Min Price</label>
                    <input className="form-control" type="number" name="minPrice" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input className="form-control" type="number" name="maxPrice" onChange={this.handleChange}></input>
                    <label>Select Date</label>
                    <input className="form-control" type="date" name="fromDate" onChange={this.handleChange}></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>Multiple Date Range</label>
                    <br></br>
                    {toDateContent}
                    {btn}
                </form>
            </>

        return (
            mainContent
        )
    }
}
