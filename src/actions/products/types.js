export const FETCH_DATA = 'FETCH_DATA';
export const SET_IS_LOADING = "SET_IS_LOADING";
export const SET_PAGE_NUMBER = "SET_PAGE_NUMBER";
export const NEW_DATA = "NEW_DATA";
