import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import './header.component.css';

const logout = (history) => {
    localStorage.clear();
    history.push('/');
    // clear storage clear
    // navigate ==> challenging props will not have history
}
const Header = function (props) {
    let menu = props.isLoggedIn
        ? <ul className="nav_menu">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/profile">Profile</Link>

            </li>
            <div className="float_right">
                <li className="nav_item">
                    <button className="btn btn-success m-r-10" onClick={() => logout(props.history)}>logout</button>
                </li>
            </div>

        </ul>
        : <ul className="nav_menu">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>
            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>

            </li>
        </ul>

    return (
        <div>
            {menu}
        </div>
    )
}
export default withRouter(Header);