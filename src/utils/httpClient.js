import axios from 'axios';

// axios base instance
const http = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    responseType: 'json'
})

const getHeaders = (isSecure = false) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecure) {
        options['Authorization'] = localStorage.getItem('token');
    }
    return options;
}


function get(url, { params = {} } = {}, isSecure = false) {
    // return new Promise((resolve, reject) => {
    return http.get(url, {
        params,
        headers: getHeaders(isSecure),
    })
    //         .then(response => {
    //             resolve(response.data);
    //         })
    //         .catch(err => {
    //             reject(err.response.data);
    //         })
    // })

}

function post(url, data = {}, { params = {} } = {}, isSecure = false) {
    return http.post(url, data, {
        headers: getHeaders(isSecure),
        params
    });
}

function remove(url, isSecure = false) {
    return http.delete(url, {
        headers: getHeaders(isSecure),
        params: {}
    });
}

function put(url, data, { params = {} } = {}, isSecure = false) {
    return http.put(
        url,
        data,
        {
            headers: getHeaders(isSecure),
            params
        });
}

function upload(method, url, data, files) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        var formData = new FormData();
        // formadata will hold all the file and text data
        if (files && files.length) {
            formData.append('img', files[0], files[0].name);
        }

        for (let key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    reject(xhr.response)
                }
            }
        }

        xhr.open(method, url, true);
        xhr.send(formData);
    })

}

export const httpClient = {
    get, post, remove, put, upload
}