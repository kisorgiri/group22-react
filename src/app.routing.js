import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import React from 'react';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import Header from './components/common/header/header.component';
import Dashboard from './components/common/dashboard/dashboard.component';
import Sidebar from './components/common/sidebar/sidebar.component';
import { NotFound } from './components/common/not-found/notfound.component';
import { ViewProductComponent } from './components/products/view-products/view-prouduct.component';
import AddProductComponent from './components/products/add-product/add-product.component';
import EditProductComponent from './components/products/edit-product/edit-product.component';
import { SearchProduct } from './components/products/search-product/search-product.component';
import { ForgotPasswordComponent } from './components/auth/forgotPassword/fogotPassword.component';
import { ResetPasswordComponent } from './components/auth/resetPassword/resetPassword.component';
import MessageComponent from './components/users/messages/message.component';

// protected route
const ProtectedRoute = ({ component: Component, ...props }) => (
    <Route {...props} render={(props) => (
        localStorage.getItem('token')
            ? <div>
                <div className="nav_bar">
                    <Header isLoggedIn={true}></Header>
                </div>
                <div className="sidenav">
                    <Sidebar user={localStorage.getItem('user')}></Sidebar>
                </div>
                <div className="main">
                    <Component {...props}></Component>
                </div>
            </div>
            : <Redirect to='/'></Redirect>  // todo pass location  of where abouts
    )}>
    </Route>
)

// public route

const PublicRoute = ({ component: Component, ...props }) => (
    <Route {...props} render={(props) => (
        <div>
            <div className="nav_bar">
                <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
            </div>
            <div className="main">
                <Component {...props}></Component>
            </div>
        </div>
    )}>
    </Route>
)
export default function AppRouting(props) {
    return (
        <Router>
            <Switch>
                <PublicRoute path="/" exact component={LoginComponent}></PublicRoute>
                <PublicRoute path="/register" component={RegisterComponent}></PublicRoute>
                <PublicRoute path="/forgot-password" component={ForgotPasswordComponent}></PublicRoute>
                <PublicRoute path="/reset-password/:token" component={ResetPasswordComponent}></PublicRoute>
                <ProtectedRoute path="/view-products" component={ViewProductComponent}></ProtectedRoute>
                <ProtectedRoute path="/add-product" component={AddProductComponent}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/edit-product/:id" component={EditProductComponent}></ProtectedRoute>
                <ProtectedRoute path="/search-product" component={SearchProduct}></ProtectedRoute>
                <ProtectedRoute path="/message" component={MessageComponent}></ProtectedRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </Router>
    )
}


// react-router-dom ==>library for creating SPA
// browserRouter ==> web wrapper for overall config
// Route ==> config block for components (path,exact)
// Switch ==> one component at a time
// Link ==> similar to anchor tag (navigation from templates)
// props ==> match history location ==> 
// Not found Componnet
