import React, { Component } from 'react'
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';
import { withRouter } from 'react-router-dom';

const IMG_URL = process.env.REACT_APP_IMG_URL;

const BaseURL = `${process.env.REACT_APP_BASE_URL}/product`
const defaultForm = {
    name: '',
    brand: '',
    description: '',
    price: '',
    color: '',
    tags: '',
    manuDate: '',
    expiryDate: '',
    quantity: '',
    category: '',
    discountedItem: false,
    discountType: '',
    discount: '',
    form_image: '',
}
class ProductFormComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isLoading: false,
            validForm: true
        }
    }
    componentDidMount() {
        if (this.props.productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.productData,
                    discountedItem: this.props.productData.discount && this.props.productData.discount.discountedItem
                        ? this.props.productData.discount.discountedItem
                        : false,
                    discountType: this.props.productData.discount && this.props.productData.discount.discountType
                        ? this.props.productData.discount.discountType
                        : '',
                    discount: this.props.productData.discount && this.props.productData.discount.discount
                        ? this.props.productData.discount.discount
                        : '',
                }
            })
        }
    }

    handleChange = e => {
        let { type, name, value, checked } = e.target;
        if (type === 'checkbox') {
            value = checked;
        }
        if (type === 'file') {
            value = e.target.files
        }

        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            // TODO form validation 
        })
    }
    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();

        if (this.state.data._id) {
            this.update();
        } else {
            this.add();
        }

    }

    update = () => {
        const { data } = this.state;
        const files = data.form_image;
        delete data.form_image
        httpClient.upload("PUT", `${BaseURL}/${this.state.data._id}?token=${localStorage.getItem('token')}`, data, files)
            .then(data => {
                notify.showInfo('Product Updated successfully');
                this.props.history.push('/view-products');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    add = () => {
        const { data } = this.state;
        const files = data.form_image;
        delete data.form_image
        httpClient.upload("POST", `${BaseURL}?token=${localStorage.getItem('token')}`, data, files)
            .then(data => {
                notify.showInfo('Product added successfully');
                this.props.history.push('/view-products');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {

        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">submit</button>

        let dicountContent = this.state.data.discountedItem
            ? <>
                <label>DiscountType</label>
                <select className="form-control" name="discountType" onChange={this.handleChange}>
                    <option disabled={true} value="selected">(Select Type)</option>
                    <option value="percentage">Percentage</option>
                    <option value="value">Value</option>
                    <option value="quantity">Quantity</option>
                </select>
                <label>Discount</label>
                <input type="text" value={this.state.data.discount} className="form-control" placeholder="Discount" name="discount" onChange={this.handleChange} ></input>
            </>
            : ''
        console.log('this.state .data image .>>', this.state.data.image);
        let previousImg = this.state.data.image
            ? <>
                <p>Image</p>
                <img src={`${IMG_URL}/${this.state.data.image}`} alt="name.png" width="400px"></img>
            </>
            : '';

        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input type="text" value={this.state.data.name} className="form-control" placeholder="Name" name="name" onChange={this.handleChange} ></input>
                    <label>Description</label>
                    <input type="text" value={this.state.data.description} className="form-control" placeholder="Description" name="description" onChange={this.handleChange} ></input>
                    <label>Category</label>
                    <input type="text" value={this.state.data.category} className="form-control" placeholder="category" name="category" onChange={this.handleChange} ></input>
                    <label>Brand</label>
                    <input type="text" value={this.state.data.brand} className="form-control" placeholder="Brand" name="brand" onChange={this.handleChange} ></input>
                    <label>Color</label>
                    <input type="text" value={this.state.data.color} className="form-control" placeholder="Color" name="color" onChange={this.handleChange} ></input>
                    <label>Price</label>
                    <input type="text" value={this.state.data.price} className="form-control" placeholder="Price" name="price" onChange={this.handleChange} ></input>
                    <label>Quantity</label>
                    <input type="text" value={this.state.data.quantity} className="form-control" placeholder="Quantity" name="quantity" onChange={this.handleChange} ></input>
                    <label>Manu Date</label>
                    <input type="date" value={this.state.data.manuDate} className="form-control" name="manuDate" onChange={this.handleChange} ></input>
                    <label>Expiry Date</label>
                    <input type="date" value={this.state.data.expiryDate} className="form-control" name="expiryDate" onChange={this.handleChange} ></input>
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    <br></br>
                    {dicountContent}
                    <br></br>
                    {previousImg}
                    <br />
                    <input type="file" name="form_image" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
            </>
        )
    }
}


export const ProductForm = withRouter(ProductFormComponent);
