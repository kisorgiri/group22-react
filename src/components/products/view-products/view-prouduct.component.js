import React, { Component } from 'react'
import notify from '../../../utils/notify';
import { LoaderComponent } from '../../common/loader/loader.component';
import Util from './../../../utils/util';
import { connect } from 'react-redux';
import { fetchProduct, changePageNumber } from './../../../actions/products/productActions';

const IMG_URL = process.env.REACT_APP_IMG_URL;

class ViewProduct extends Component {
    // constructor() {
    //     super();

    // }

    componentDidMount() {
        this.httpCall();
        // if (this.props.productData) {
        // TODO add another redux cycle to update products
        //     this.setState({
        //         products: this.props.productData
        //     })
        // }
        // else {
        //     this.httpCall(this.state.pageNumber, this.state.pageSize);
        // }
    }

    // componentDidUpdate(previousProps) {

    // }

    httpCall(pageNumber = 1) {
        // this.setState({
        //     isLoading: true
        // })
        this.props.fetch(pageNumber, this.props.pageSize);

    }

    remove(id, index) {
        // eslint-disable-next-line no-restricted-globals
        // let confirmation = confirm('are you sure, you want to delete?')
        // if (confirmation) {
        //     httpClient.remove(`/product/${id}`, true)
        //         .then(data => {
        //             notify.showInfo('Product deleted');
        //             const { products } = this.state;
        //             products.splice(index, 1);
        //             this.setState({
        //                 products
        //             });
        //         })
        //         .catch(err => {
        //             notify.handleError(err);
        //         })
        // }
    }

    next = () => {
        let { pageNumber } = this.props;
        pageNumber += 1;
        this.props.changePage(pageNumber);
        this.httpCall(pageNumber);

    }
    previous = () => {
        let { pageNumber } = this.props;
        pageNumber -= 1;
        this.props.changePage(pageNumber);
        this.httpCall(pageNumber);
    }

    editProduct(id) {
        this.props.history.push(`/edit-product/${id}`);
    }

    render() {
        let tableContent = (this.props.products || []).map((item, i) => (
            <tr key={item._id}>
                <td>{i + 1}</td>
                <td>{item.name}</td>
                <td>{item.category}</td>
                <td>{item.brand}</td>
                <td>{item.price}</td>
                <td>{Util.formatDate(item.createdAt)}</td>
                <td>{Util.formatTime(item.createdAt)}</td>
                <td>
                    <img src={`${IMG_URL}/${item.image}`} alt="product.jpg" width="200px"></img>
                </td>
                <td>
                    <button className="btn btn-info" onClick={() => this.editProduct(item._id)}>
                        edit
                    </button>
                    <button className="btn btn-danger" onClick={() => this.remove(item._id, i)}>delete</button>
                </td>
            </tr >
        ))
        let pagination = this.props.productData
            ? ''
            : <div>
                <button onClick={this.previous} className="btn btn-success">previous</button>
                <button onClick={this.next} className="btn btn-success">next</button>
            </div>
        let mainDiv = this.props.isLoading
            ? <LoaderComponent></LoaderComponent>
            : <>
                <table className="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Price</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Image</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableContent}
                    </tbody>
                </table>
                {pagination}
            </>

        return (
            <>
                <h2>View Products</h2>
                {mainDiv}
            </>
        )
    }
}
// mapStateToProps
// incoming data for component
const mapStateToProps = (state) => ({
    isLoading: state.product.isLoading,
    products: state.product.products,
    pageNumber: state.product.pageNumber,
    pageSize: state.product.pageSize
})

// mapDispatchToProps
// outgoing events/actions from component
const mapDispatchToProps = (dispatch) => ({
    fetch: (pageNumber) => { dispatch(fetchProduct(pageNumber)) },
    changePage: (pageNumber) => { dispatch(changePageNumber(pageNumber)) }
})

export const ViewProductComponent = connect(mapStateToProps, mapDispatchToProps)(ViewProduct);
