import React from 'react';
import { Link } from 'react-router-dom';
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';

export class ResetPasswordComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            isSubmitting: false,
            validForm: true,
            data: {
                password: '',
                cPassword: ''
            }
        }
    }

    componentDidMount() {
        this.resetToken = this.props.match.params['token'];
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }
    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        httpClient.post(`/auth/reset-password/${this.resetToken}`, this.state.data, {})
            .then(data => {
                notify.showInfo("Password reset successful Please login to continue");
                this.props.history.push('/');
            })
            .catch(err => notify.handleError(err))
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">submit</button>
        return (
            <>
                <h2>Reset Password</h2>
                <p>Enter your New Password </p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input type="password" placeholder="Password" name="password" className="form-control" onChange={this.handleChange}></input>
                    <label>Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" name="cPassword" className="form-control" onChange={this.handleChange}></input>
                    <br></br>
                    {btn}
                </form>
                <p>back to <Link to="/">login</Link></p>
            </>
        )
    }
}