import moment from 'moment';

function formatDate(date) {
    return moment(date).format('YYYY-MM-DD');
}

function formatTime(date) {
    return moment(date).format('hh:mm a');
}

// function redirectToDashboard(props, role) {
//     // props with history
//     if (role === '1') {
//         this.history.push('/admin-dashboard')
//     }
//     if (role === '2') {
//         this.history.push('/home')
//     }
//     if (role === '3') {
//         this.history.push('/dashboard')
//     }
// }
// function redirectToLogin(){
//         // props with history
//         // redirect to login url

// }

export default {
    formatDate,
    formatTime
}