import React, { Component } from 'react'

export default class Dashboard extends Component {
    componentDidMount() {
        console.log('this.props >>', this.props.match.params['name']);
        console.log('check local storage >>', localStorage.getItem('token'));
        console.log('check user storage >>', JSON.parse(localStorage.getItem('user')));
    }
    render() {
        return (
            <div>
                <p>Welcome to Group22 Store</p>
                <p>Please use side navigation menu or contact system administrator for support</p>
            </div>
        )
    }
}
