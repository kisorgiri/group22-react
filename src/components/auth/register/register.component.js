import React, { Component } from 'react';
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';
const defaultForm = {
    name: '',
    username: '',
    password: '',
    email: '',
    dob: '',
    phoneNumber: '',
    gender: '',
    address: ''
}

export class RegisterComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: { ...defaultForm },
            error: {
                ...defaultForm,
            },
            isSubmitting: false,
            validForm: false
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg;

        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : `${fieldName} is requried`
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'weak Password'
                    : `${fieldName} is requried`
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@')
                        ? ''
                        : 'invalid email address'
                    : `${fieldName} is requried`
                break;

            default:
                break;
        }
        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            this.checkFormValid();
        })
    }

    checkFormValid() {
        let errors = Object
            .values(this.state.error)
            .filter(err => err);

        let validForm = errors.length === 0;
        this.setState({
            validForm
        });
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        // data is this.state.data
        httpClient.post('/auth/register', this.state.data)
            .then((data) => {
                notify.showInfo('Registration Successfull please login')
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err)
                this.setState({
                    isSubmitting: true
                })
            });
    }


    render() {

        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">submit</button>
        return (
            <div>
                <h2>Register</h2>
                <p>Please register to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input className="form-control" placeholder="Name" name="name" onChange={this.handleChange} ></input>
                    <label>Email</label>
                    <input type="text" className="form-control" placeholder="Email" name="email" onChange={this.handleChange} ></input>
                    <p className="validation_error">{this.state.error.email}</p>
                    <label>Address</label>
                    <input type="text" className="form-control" placeholder="Address" name="address" onChange={this.handleChange} ></input>
                    <label>Phone Number</label>
                    <input type="text" className="form-control" placeholder="Phone Number" name="phoneNumber" onChange={this.handleChange} ></input>
                    <label>Username</label>
                    <input type="text" className="form-control" placeholder="Username" name="username" onChange={this.handleChange} ></input>
                    <p className="validation_error">{this.state.error.username}</p>

                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Password" name="password" onChange={this.handleChange} ></input>
                    <p className="validation_error">{this.state.error.password}</p>

                    <label>Date Of Birth</label>
                    <input type="date" className="form-control" name="dob" onChange={this.handleChange} ></input>
                    <label>Gender</label>
                    <br></br>
                    <input type="radio" name="gender" value="male"></input> male
                    <input type="radio" name="gender" value="female"></input> female
                    <input type="radio" name="gender" value="others"></input> others
                    <br></br>
                    {btn}
                </form>
            </div>
        )
    }
}