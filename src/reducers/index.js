import ProductReducers from './productReducer';
import { combineReducers } from 'redux';

export default combineReducers({
    product: ProductReducers
});
