import React from 'react';
import './sidebar.component.css';
import { Link } from 'react-router-dom';

const Sidebar = (props) => {
    return (
        <div>
            <Link to='/dashboard'>Home</Link>
            <Link to='/add-product'>Add Product</Link>
            <Link to='/view-products'>View Product</Link>
            <Link to='/search-product'>Search Product</Link>
            <Link to='/message'>Messages</Link>
        </div>
    )
}

export default Sidebar;