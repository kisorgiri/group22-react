import { SET_IS_LOADING, FETCH_DATA, SET_PAGE_NUMBER } from './../actions/products/types';

const initialState = {
    products: [],
    isLoading: false,
    pageSize: 10,
    pageNumber: 1
}
function ProductReducers(state = initialState, action) {
    console.log('here at reducers,', action);
    // action will have type payload
    //we will  use switch case to change the state with given payload and type

    switch (action.type) {
        case FETCH_DATA:
            return {
                ...state,
                products: action.payload
            }

        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }

        default:
            return state;

    }


}

export default ProductReducers;