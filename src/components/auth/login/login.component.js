import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { httpClient } from './../../../utils/httpClient';
import notify from './../../../utils/notify';
export class LoginComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                username: '',
                password: ''
            },
            error: {
                username: '',
                password: '',
            },
            isSubmitting: false,
            validForm: true
        };
    }

    handleChange(e) {
        const { name, value } = e.target;

        // this.setState({
        //     [name]: value
        // })

        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg;
        errMsg = this.state.data[fieldName]
            ? ''
            : `${fieldName} is required`;

        this.setState((preState) => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            // logic to disbale button
        })

    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        });
        httpClient.post('/auth/login', this.state.data)
            .then(response => {
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                // webstorage
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                this.props.history.push(`/dashboard`);
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button className="btn btn-info" disabled={true}>Logingin...</button>
            : <button className="btn btn-primary" disabled={!this.state.validForm} type="submit">Login</button>

        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate={true}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" name="username" placeholder="Username" id="username" onChange={this.handleChange.bind(this)} required={true}></input>
                    <p className="validation_error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" placeholder="*******" id="password" onChange={this.handleChange.bind(this)} required={true}></input>
                    <p className="validation_error">{this.state.error.password}</p>
                    <br></br>
                    {btn}
                </form>
                <p>Don't have an account?
                    Register  <Link to="/register">here</Link></p>
                <p><Link to="/forgot-password">forgot password?</Link></p>
            </div>
        )
    }
}