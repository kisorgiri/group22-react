import React, { Component } from 'react'
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';
import { LoaderComponent } from '../../common/loader/loader.component';
import { ProductForm } from '../product-form/product-form.component';

export default class EditProductComponent extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            product: {},
        }
    }

    componentDidMount() {
        const productId = this.props.match.params['id'];
        this.setState({
            isLoading: true
        })
        httpClient.get(`/product/${productId}`, {}, true)
            .then(data => {
                // console.log('product >>', data);
                this.setState({
                    product: data.data
                });
            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }
    render() {
        let content = this.state.isLoading
            ? <LoaderComponent></LoaderComponent>
            : <>
                <ProductForm title="Update Product" productData={this.state.product}></ProductForm>
            </>
        return (
            content
        )
    }
}
