import { FETCH_DATA, SET_IS_LOADING, SET_PAGE_NUMBER } from './types';
import { httpClient } from '../../utils/httpClient';
import notify from '../../utils/notify';

export const fetchProduct = (pageNumber = 1, pageSize = 10) => (dispatch) => {
    dispatch(loading(true));
    httpClient.get('/product', {
        params: {
            pageNumber,
            pageSize
        }
    }, true)
        .then(response => {
            console.log('thunk helps to dispatch after certain delay');
            dispatch({
                type: FETCH_DATA,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
        .finally(() => {
            dispatch(loading(false))
        })
}

export const loading = (isloading) => ({
    type: SET_IS_LOADING,
    payload: isloading
})

export const changePageNumber = (pageNumber) => (dispatch) => {
    dispatch({
        type: SET_PAGE_NUMBER,
        payload: pageNumber
    })
}

