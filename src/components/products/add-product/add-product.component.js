import React, { Component } from 'react'
import { ProductForm } from '../product-form/product-form.component'

export default class AddProductComponent extends Component {

    add() {
        // this should be called from product form component and http call must be made here
    }
    render() {
        return (
            <ProductForm title="Add Product"></ProductForm>
        )
    }
}
