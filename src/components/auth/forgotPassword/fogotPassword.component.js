import React from 'react';
import { Link } from 'react-router-dom';
import { httpClient } from '../../../utils/httpClient';
import notify from '../../../utils/notify';

export class ForgotPasswordComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            isSubmitting: false,
            validForm: true,
            data: {
                email: ''
            }
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            data: {
                [name]: value
            }
        })
    }
    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        httpClient.post('/auth/forgot-password', this.state.data, {})
            .then(data => {
                notify.showInfo("Password reset link sent to your email please check your inbox");
                this.props.history.push('/');
            })
            .catch(err => notify.handleError(err))
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">submit</button>
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please provide your email address to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Email</label>
                    <input type="text" placeholder="Email" name="email" className="form-control" onChange={this.handleChange}></input>
                    <br></br>
                    {btn}
                </form>
                <p>back to <Link to="/">login</Link></p>
            </>
        )
    }
}