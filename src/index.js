// es6 import
import React from 'react';
import ReactDOM from 'react-dom';

// load app (main file)
import { App } from './app';

ReactDOM.render(<App name="Kishor" address="tinknue" />, document.getElementById('app'));